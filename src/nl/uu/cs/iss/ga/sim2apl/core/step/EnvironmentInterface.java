package nl.uu.cs.iss.ga.sim2apl.core.step;

import nl.uu.cs.iss.ga.sim2apl.core.deliberation.DeliberationResult;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Future;

public interface EnvironmentInterface<T> {

    void stepStarting(long startingTimeStep);

    void stepFinished(long finishedTimeStep, int timeStepDuration, List<Future<DeliberationResult<T>>> producedAgentActions);

    void simulationFinished(long lastTimeStep, int lastTimeStepDuration);
}
