package nl.uu.cs.iss.ga.sim2apl.core.step;

public interface SimulationEngine<T> {

    /**
     * This method starts the simulation.
     * <p>
     * This method should be implemented in such a way that before a new time step is
     * started, the stepStarting methods of all registered EnvironmentInterfaces are
     * allowed to run in a blocking manner, and after the time step is finished the
     * stepFinished methods are allowed to run in a blocking manner.
     * <p>
     * When the simulation is finished, this method should allow the
     * simulationFinished method to run in a blocking manner for all environment interfaces.
     *
     * @return True after the entire simulation is finished
     */
    boolean start();

    /**
     * Registers a new environmentInterface. Each of these will
     * be allowed to perform their stepStarting and stepFinished methods
     * before and after a new time step will be started
     *
     * @param environmentInterface Environment interface to register
     */
    void registerEnvironmentInterface(EnvironmentInterface<T> environmentInterface);

    /**
     * Deregisters an environment interface from being notified of stepStarting
     * and stepFinished events
     *
     * @param environmentInterface The environment interface to deregister
     */
    void deregisterEnvironmentInterface(EnvironmentInterface<T> environmentInterface);
}
