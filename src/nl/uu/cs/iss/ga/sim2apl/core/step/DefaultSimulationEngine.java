package nl.uu.cs.iss.ga.sim2apl.core.step;

import nl.uu.cs.iss.ga.sim2apl.core.deliberation.DeliberationResult;
import nl.uu.cs.iss.ga.sim2apl.core.platform.Platform;

import java.util.List;
import java.util.concurrent.Future;

/**
 * The default simulation engine starts the simulation, and requests the step executor to advance
 * immediately after the previous time step has finished.
 *
 * The execution of the stepStarting and stepFinished methods, as well as the agent deliberation phase are deliberately
 * made blocking. This ensures that the environment is only updated after <i>all</i> agents have finished their
 * sense-reason-act cycle, as well as that the agents only start a new cycle when the environment has completely
 * finished processing.
 *
 * The sense-reason-act cycles are executed using an ExecutorService, since agents are allowed to run in parallel.
 * This speeds up the time it takes to perform one deliberation cycle for all the agents.
 */
public class DefaultSimulationEngine<T> extends AbstractSimulationEngine<T> {

    /** The StepExecutor is obtained from the platform. By default, the DefaultStepExecutor is used, but this can
     * be overridden by specifying a custom StepExecutor at platform creation */
    private final StepExecutor<T> executor;

    /**
     * {@inheritDoc}
     */
    public DefaultSimulationEngine(Platform platform, int nIterations, EnvironmentInterface<T>... environmentInterfaces) {
        super(platform, nIterations, environmentInterfaces);
        this.executor = platform.getStepExecutor();
    }

    /**
     * {@inheritDoc}
     */
    public DefaultSimulationEngine(Platform platform) {
        super(platform);
        this.executor = platform.getStepExecutor();
    }

    /**
     * {@inheritDoc}
     */
    public DefaultSimulationEngine(Platform platform, EnvironmentInterface<T>... processors) {
        super(platform, processors);
        this.executor = platform.getStepExecutor();
    }

    /**
     * {@inheritDoc}
     */
    public DefaultSimulationEngine(Platform platform, int iterations) {
        super(platform, iterations);
        this.executor = platform.getStepExecutor();
    }

    /**
     * {@inheritDoc}
     */
    public boolean start() {
        if(this.nIterations <= 0) {
            // Run until actively interrupted
            while(true) doTimeStep();
        } else {
            // Run for fixed number of time steps
            for (int i = 0; i < this.nIterations; i++) doTimeStep();
        }
        this.processSimulationFinishedHook(this.nIterations, executor.getLastTimeStepDuration());
        this.executor.shutdown();
        return true;
    }

    /**
     * Performs a single time step, and notifies all Environment Interfaces before and after the time step execution
     */
    private void doTimeStep() {
        int timeStep = this.executor.getCurrentTimeStep();
        this.processStepStarting(timeStep);
        List<Future<DeliberationResult<T>>> agentActions = this.executor.doTimeStep();
        this.processStepFinished(timeStep, executor.getLastTimeStepDuration(), agentActions);
    }
}
