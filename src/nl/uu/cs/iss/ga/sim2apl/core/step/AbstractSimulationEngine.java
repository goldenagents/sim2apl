package nl.uu.cs.iss.ga.sim2apl.core.step;

import nl.uu.cs.iss.ga.sim2apl.core.deliberation.DeliberationResult;
import nl.uu.cs.iss.ga.sim2apl.core.platform.Platform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;

/**
 * An abstract simulation engine, that only leaves the initiation of a time step
 * to the programmer.
 *
 * Before and after each time step, as well as at the end of the simulation, the
 * appropriate methods from each registered environment interface are called.
 */
public abstract class AbstractSimulationEngine<T> implements SimulationEngine<T> {

    protected List<EnvironmentInterface<T>> environmentInterfaceList;
    protected int nIterations;
    protected final Platform platform;

    /**
     * Instantiate this class with only a platform
     * @param platform  Platform
     */
    public AbstractSimulationEngine(Platform platform) {
        this.environmentInterfaceList = new ArrayList<>();
        this.platform = platform;
        this.nIterations = -1;
    }

    /**
     * Instantiate this class with a platform and an initial (set of) EnvironmentInterface(s)
     * @param platform                  Platform
     * @param environmentInterfaces     environment interfaces
     */
    public AbstractSimulationEngine(Platform platform, EnvironmentInterface<T>... environmentInterfaces) {
        this(platform);
        this.environmentInterfaceList.addAll(Arrays.asList(environmentInterfaces));
    }

    /**
     * Instantiate this class with a platform and with a specification of how many time steps to run
     *
     * @param platform      Platform
     * @param iterations    Number of time steps to run
     */
    public AbstractSimulationEngine(Platform platform, int iterations) {
        this(platform);
        this.nIterations = iterations;
    }

    /**
     * Instantiate this class with a platform, a specification of how many time steps to run, and
     * an initial (set of) EnvironmentInterface(s)
     * @param platform      Platform
     * @param iterations    Number of time steps to run
     * @param processors    environment interfaces
     */
    public AbstractSimulationEngine(Platform platform, int iterations, EnvironmentInterface<T>... processors) {
        this(platform, processors);
        this.nIterations = iterations;
    }

    /**
     * Run the step starting method of all registered EnvironmentInterfaces in a blocking manner
     *
     * @param startingTimeStep  The time step that will be started
     */
    protected void processStepStarting(int startingTimeStep) {
        this.environmentInterfaceList.forEach(tph -> tph.stepStarting(startingTimeStep));
    }

    /**
     * Run the step finished method of all registered EnvironmentInterfaces in a blocking manner
     *
     * @param finishedTimeStep      The time step that has finished
     * @param lastTimeStepDuration  The computation time used by the last time step in milliseconds
     * @param actions           A collection of future deliberation results. All futures in this collection
     *                          can be assumed to have finished calculation, so .get() can always be called, unless
     *                          an execution exception occurred in the deliberation cycle of the corresponding agent.
     */
    protected void processStepFinished(int finishedTimeStep, int lastTimeStepDuration, List<Future<DeliberationResult<T>>> actions) {
        this.environmentInterfaceList.forEach(tph -> tph.stepFinished(finishedTimeStep, lastTimeStepDuration, actions));
    }

    /**
     * Run the simulationFinished method of all registered EnvironmentInterfaces in a blocking manner
     *
     * @param lastTimeStep          The last executed time step before the simulation finished
     * @param lastTimeStepDuration  The computation time used by the last time step in milliseconds
     */
    protected void processSimulationFinishedHook(int lastTimeStep, int lastTimeStepDuration) {
        this.environmentInterfaceList.forEach(thp -> thp.simulationFinished(lastTimeStep, lastTimeStepDuration));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void registerEnvironmentInterface(EnvironmentInterface<T> environmentInterface) {
        if(!this.environmentInterfaceList.contains(environmentInterface)) {
            this.environmentInterfaceList.add(environmentInterface);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void deregisterEnvironmentInterface(EnvironmentInterface<T> processor) {
        this.environmentInterfaceList.remove(processor);
    }
}
